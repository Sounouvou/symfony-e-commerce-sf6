<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Security\UserAuthenticator;
use App\Service\JWTService;
use App\Service\SendMailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\UserRepository;


class RegistrationController extends AbstractController
{
    #[Route('/inscription', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, Security $security, EntityManagerInterface $entityManager,
    SendMailService $mail, JWTService $jwt): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email

            // on genere le JWT de l'utilisateur
            //on cree le header

            $header = [
                'typ'=>'JWT',
                'alg'=> 'HS256'
            ];

            //on cree le Paylaod

            $payload = [
                'user_id'=>$user->getId()
            ];

            // on genere le token

            $token = $jwt ->generate($header,$payload,
            $this->getParameter('app.jwtsecret'));

            //on envoie un mail
            $mail->send(
                'no-replay@monsite.net',
                $user->getEmail(),
                'Activation de votre compte sur le site e-commerce',
                'register',
                compact('user','token')
            );
            return $security->login($user, UserAuthenticator::class, 'main');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route('/verif/{token}', name:'verify_user')]
    public function verifyUser($token, JWTService $jwt, UserRepository $usersRepository, EntityManagerInterface $em): Response
    {
        // on verifie si le token est valide, n'a pas expiré, n'a 
        // pas été modifier
        if($jwt->isValid($token)&& !$jwt->isExpired($token) && 
        $jwt->check($token,$this->getParameter('app.jwtsecret'))){
            
            // on recupere le payload
            $payload =$jwt->getPayload($token);

            // on recupere le user du token
            $user = $usersRepository->find($payload['user_id']);

            // on verifie que l'utilisateur existe et n'a
            // pas encore activer son compte
            if($user && !$user->getIsverified()){
                $user->setIsVerified(true);
                $em->flush();
                $this->addFlash('success', 'utilisateur activé');
                return $this->redirectToRoute('profile_index');
            }
        }

        // ici un probleme se pose dans le token
        $this->addFlash('danger', 'le token est invalide ou a expiré');
        return $this->redirectToRoute('app_login');
    }

        #[Route('/renvoiverif', name: 'resend_verif')]
        public function resendVerif(JWTService $jwt,SendMailService  $mail, UserRepository $usersRepository): Response
        {
            $user =$this->getUser();
            $user = $usersRepository->findOneBy(["email"=>$user->getUserIdentifier()]);

            if(!$user){
                $this->addFlash('danger','Vous devez etre connecté
                pour accéder a cette page');
                return $this->redirectToRoute('app_login');
            }

            if($user->getIsverified()){
                $this->addFlash('warning','cet utilisateur est deja
                active');
                return $this->redirectToRoute('profile_index');
            }

            // on genere le JWT de l'utilisateur
            //on cree le header

            $header = [
                'typ'=>'JWT',
                'alg'=> 'HS256'
            ];

            //on cree le Paylaod maintenant

            $payload = [
                'user_id'=>$user->getId()
            ];

            // on genere le token

            $token = $jwt ->generate($header,$payload,
            $this->getParameter('app.jwtsecret'));

            //on envoie un mail
            $mail->send(
                'no-replay@monsite.net',
                $user->getEmail(),
                'Activation de votre compte sur le site e-commerce',
                'register',
                compact('user','token')
            );

            $this->addFlash('success','Email de verification envoyé');
            return $this->redirectToRoute('profile_index');
        }
    
}

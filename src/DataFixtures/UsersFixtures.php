<?php

namespace App\DataFixtures;

use App\Entity\User; 
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\String\Slugger\SluggerInterface;
use Faker;
class UsersFixtures extends Fixture
{

    public function __construct(
        private UserPasswordHasherInterface $passwordEncoder,
        private SluggerInterface $slugger){}

    public function load(ObjectManager $manager): void
    {
        
        $admin = new User();
        $admin->setEmail("admin.fr");
        $admin->setlastname("richard");
        $admin->setfirstname("sounouvou");
        $admin->setaddress("'12 rue du port");
        $admin->setzipcode("'12345");
        $admin->setcity("'Paris");
        $admin->setPassword(
            $this->passwordEncoder->hashPassword($admin, 'admin')
        );
        $admin->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);

        $Faker = Faker\Factory::create('fr_FR');

        for($usr= 1; $usr <= 5; $usr++)
        {
            $user = new User();
            $user->setEmail($Faker->email);
            $user->setlastname($Faker->lastName);
            $user->setfirstname($Faker->firstName);
            $user->setaddress($Faker->streetaddress);
            $user->setzipcode(str_replace(' ', '', $Faker->postcode));
            $user->setcity($Faker->city);
            $user->setPassword(
                $this->passwordEncoder->hashPassword($user, 'secret')
            );

            $manager->persist($admin);
        }
        $manager->flush();
    }
}

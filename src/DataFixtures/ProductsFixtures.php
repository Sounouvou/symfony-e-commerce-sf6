<?php

namespace App\DataFixtures;


use App\Entity\Products; 
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\SluggerInterface;
use Faker;

class ProductsFixtures extends Fixture
{
    public function __construct(private SluggerInterface $slugger){}
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        for($prod =1; $prod <= 10 ; $prod++)
        {
            $products = new Products();
            $products->setname($faker->text(5));
            $products->setDescription($faker->text());
            $products->setSlug($this->slugger->slug($products->getName())->lower());
            $products->setprice($faker->numberBetween(900, 15000));
            $products->setstock($faker->numberBetween(0,10));

            //on va chercher une reference de categorie

            $category = $this->getReference('cat-'.rand(1,4));
            $products->setCategories($category);
            $this->setReference('prod-'.$prod, $products); 
            
      
            $manager->persist($products);
            $this->addReference('prod-'.$prod, $products);

        }

        $manager->flush();
    }
}

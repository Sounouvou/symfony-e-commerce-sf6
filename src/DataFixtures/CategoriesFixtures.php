<?php

namespace App\DataFixtures;

use App\Entity\Categories; 
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\SluggerInterface;

class CategoriesFixtures extends Fixture
{
    private $counter = 1; // Compteur pour créer des références uniques pour les catégories

    // Injection de dépendance pour le service SluggerInterface
    public function __construct(private SluggerInterface $slugger) {}

    // Méthode appelée pour charger les fixtures
    public function load(ObjectManager $manager): void
    {
        // Création d'une catégorie parent 'Informatique'
        $parent = $this->createCategory('Informatique', null, $manager);

        // Création de sous-catégories avec 'Informatique' comme parent
        $this->createCategory("Ordinateurs portables", $parent, $manager);
        $this->createCategory("Ecrans", $parent, $manager);
        $this->createCategory("Souris", $parent, $manager);

        // Persiste toutes les catégories dans la base de données
        $manager->flush();
    }  

    // Méthode pour créer une catégorie
    public function createCategory(string $name, Categories $parent = null, ObjectManager $manager): Categories
    {
        $category = new Categories(); // Création d'une nouvelle instance de la catégorie
        $category->setName($name); // Définir le nom de la catégorie
        // Génération d'un slug à partir du nom de la catégorie et le convertir en minuscule
        $category->setSlug($this->slugger->slug($category->getName())->lower());
        $category->setParent($parent); // Définir la catégorie parent
        $manager->persist($category); // Persiste la catégorie dans l'ObjectManager

        // Ajouter une référence pour pouvoir accéder à cette catégorie dans d'autres fixtures
        $this->addReference('cat-' . $this->counter, $category);
        $this->counter++; // Incrémenter le compteur pour la prochaine référence

        return $category; // Retourne la catégorie créée
    }
}
